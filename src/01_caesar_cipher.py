#!/usr/bin/env python
# coding: utf-8

# # Caesar Cipher

# ![title](http://www.maths-resources.net/enrich/codes/caesar/images/caesarwheel3.gif)
# 
# The letters on the outer circle represent letters in the original text (message). The letters on the inner circle represent the (encoded) cipher text.
# 
# Here, the inner circle is rotated to the left by 3 (`k`=3). 

# ### The `chr()` and `ord()` Functions

# _`ord(c)`: Returns an integer representing the Unicode code point of the character `c`._

# In[ ]:


ord('A')


# _`chr(i)`: Returns a string of one character whose ASCII code is the integer `i`._

# In[ ]:


chr(65)


# In[ ]:


k = 3

chr(65 + k)


# In[ ]:


ord(chr(65 + k))


# _The `ord()` and `chr()` functions are the opposite of each other._

# ### Caesar cipher in Python

# 1. Define a message text, and a key.

# In[ ]:


message = 'Et tu, Brute?'

k = 3


# Print the message and key.

# In[ ]:


print ('The message is:', message)

print ('The key is:', k)


# _We will use this key to encrypt the message._

# 2. Encrypt the message, one character at a time

# In[ ]:


for input_char in message:
    
    # print this letter
    print ('Input character:', input_char)
    
    # retrieve the ASCII code for this character
    num = ord(input_char)
    
    # add the key to that code to encrupt this character
    num = num + k
    
    # retrieve the character for that ASCII code
    encrypted_char = chr(num)

    # print the encrypted letter
    print ('Encrypted character:', encrypted_char)


# _Let's encrypt only letters in the alphabet, and ignore special character like exclamation points and spaces. We can use `isalpha()` function for this purpose._

# In[ ]:


mychar = 'V'

mychar.isalpha()


# In[ ]:


mychar = '?'

mychar.isalpha()


# 3. Encrypt the message, one character at a time. Ignore special characters.

# In[ ]:


for input_char in message:
    
    # check if this character is a letter
    if input_char.isalpha():
        
        # retrieve the ASCII code for this letter 
        num = ord(input_char)
        
        # add the key to that code to encrupt this letter
        num += k
        
        # retrieve the character for that ASCII code
        encrypted_char = chr(num)
    
    else:
        
        # if special character, keep it as it is
        encrypted_char = input_char
    
    print (input_char, '-->', encrypted_char)


# _Notice how `num = num + k` can also be written as `num += k`._

# 4. Save the encypted message in a single string.

# In[ ]:


# initialize the output (encrypted) message as an empty string
encrypted_message = ''

for input_char in message:
    
    # check if this character is a letter
    if input_char.isalpha():
        
        # retrieve the ASCII code for this letter         
        num = ord(input_char)
        
        # add the key to that code to encrupt this letter
        num += k
        
        # append the encrypted char to the encrypted message string
        encrypted_message += chr(num)
        
    else:
        
        # if special character, append the original character
        encrypted_message += input_char


# In[ ]:


print ('Input message:', message)

print ('Encrypted message:', encrypted_message)


# _Let's try a different key._

# In[ ]:


# define a new key
k = 5

# initialize the output (encrypted) message as an empty string
encrypted_message = ''

for input_char in message:
    
    # check if this character is a letter
    if input_char.isalpha():
        
        # retrieve the ASCII code for this letter         
        num = ord(input_char)
        
        # add the key to that code to encrupt this letter
        num += k
        
        # append the encrypted char to the encrypted message string
        encrypted_message += chr(num)
        
    else:
        
        # if special character, append the original character
        encrypted_message += input_char

print ('Input message:', message)
print ('Encrypted message:', encrypted_message)


# _If you want to try different messages and different keys, it's useful to create a **function**._

# 4. Let's create a function!

# In[ ]:


def encrypt_message(in_message):

    # initialize the output (encrypted) message
    out_message = ''

    for in_char in in_message:
        
        if in_char.isalpha():
            
            # if letter, encrypt it
            out_message += chr(ord(in_char) + k)
        
        else:
            
            # otherwise, keep it as is
            out_message += in_char

    return out_message


# In[ ]:


type(encrypt_message)


# _`encrypt_message` is a user defined function (UDF). Python also has a lot of built-in functions._

# In[ ]:


type(print)


# In[ ]:


# call the function to encrypt the message

encrypted_msg = encrypt_message(message)


# In[ ]:


# print the encrypted message

print ('Original message:', message)

print ('Encrypted message:', encrypted_msg)


# **EXERCISE:** Modify the function to include</i> `k` <i>as one of its parameters.

# In[ ]:


def encrypt_message(in_message, key):

    # initialize the output (encrypted) message
    out_message = ''

    for in_char in in_message:
        
        if in_char.isalpha():
            
            # if letter, encrypt it
            out_message += chr(ord(in_char) + key)
        
        else:
            
            # otherwise, keep it as is
            out_message += in_char

    return out_message

print ('Original message:', message)

print ('Encrypted message (k=3):', encrypt_message(message, 3))

print ('Encrypted message (k=7):', encrypt_message(message, 7))

print ('Encrypted message (k=0):', encrypt_message(message, 0))


# _Encrypt a new message using this function._

# In[ ]:


print ('Encrypted message (k=0):', encrypt_message('Virginia Commonwealth University', 3))


# _Notice that for `k=3`, letter 'y' would get encrypted into the the pipe symbol '|'. See the ASCII table below for reference._

# ![image](http://www.asciitable.com/index/asciifull.gif)

# _Let's modify the function to avoid situations where the encrypted message contains non-alphabetic character(s). In other words, force the encryption to "wrap around" to the beginning of the alphabet if it encounters non-alphabetic characters._

# 5. Encrypt message using a function. Avoid special characters in the encrypted message.

# In[ ]:


def encrypt_message(in_message, key):

    # Initialize the output (encrypted) message
    out_message = ''

    for in_char in in_message:
        
        if in_char.isalpha():
            
            # if letter, encrypt it
            num = ord(in_char) + key
            
            # if the encrypted char is a special char,
            #  then subtract 26 to wrap around to the beginning of the alphabet
            
            if in_char.isupper() and num > ord('Z'):
                num -= 26
                
            elif in_char.islower() and num > ord('z'):
                num -= 26
            
            # append the encrypted letter to the output string
            out_message += chr(num)
            
        else:
            
            # if not a letter, append to the ouput string as is
            out_message += in_char

    return out_message


# In[ ]:


print ('Original message:', message)

print ('Encrypted message (k=3):', encrypt_message(message, 3))

print ('Encrypted message (k=3):', encrypt_message(message, 7))


# 6. Write a function to _decode_ an encrypted message using a key.

# In[ ]:


def decrypt_message(in_message, key):

    # initialize the output (decrypted) message
    out_message = ''

    for in_char in in_message:
        
        if in_char.isalpha():
            
            num = ord(in_char)
            
            # for decrypting the message, we need to substract the key
            num -= key
            
            # add the decrypted letter to the output
            out_message += chr(num)
        
        else:
            
            out_message += in_char

    return out_message


# In[ ]:


encrypted_message = 'Hw wx, Euxwh?'

print ('Decrypted message:', decrypt_message(encrypted_message, 3))

